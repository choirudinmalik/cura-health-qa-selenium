package webtesting;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AppointmentTest {
    WebDriver webDriver;

    @BeforeClass
    public void openBrowser() {
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    }

    @AfterClass
    public void closeBrowser() throws InterruptedException {
        Thread.sleep(1000);
        webDriver.quit();
    }

    @BeforeMethod
    public void login() {
        webDriver.get("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        webDriver.findElement(By.id("txt-username")).sendKeys("John Doe");
        webDriver.findElement(By.id("txt-password")).sendKeys("ThisIsNotAPassword");
        webDriver.findElement(By.id("btn-login")).click();
    }

    @AfterMethod
    public void logout() throws InterruptedException {
        Thread.sleep(1000);
        webDriver.get("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
    }

    // Make Appointment Success
    @Test(priority = 0)
    public void TC_APPOINTMENT_01() {
        // Dropdown
        Select facilitySelect = new Select(webDriver.findElement(By.id("combo_facility")));
        facilitySelect.selectByValue("Hongkong CURA Healthcare Center");

        // Checkbox
        webDriver.findElement(By.id("chk_hospotal_readmission")).click();

        // Radio
        webDriver.findElement(By.id("radio_program_medicaid")).click();
        
        // DatePicker
        webDriver.findElement(By.id("txt_visit_date")).sendKeys("15/12/2022");

        // Textarea
        webDriver.findElement(By.id("txt_comment")).sendKeys("Test");

        // Button
        webDriver.findElement(By.id("btn-book-appointment")).click();

        // Assertion
        // Url
        String currentURL = webDriver.getCurrentUrl();
        Assert.assertEquals(currentURL, "https://katalon-demo-cura.herokuapp.com/appointment.php#summary");
        
        // Header
        String headerText = webDriver.findElement(By.xpath("//*[@id='summary']/div/div/div[1]/h2")).getText();
        Assert.assertEquals(headerText, "Appointment Confirmation");

        // Facility
        String facilityText = webDriver.findElement(By.id("facility")).getText();
        Assert.assertEquals(facilityText, "Hongkong CURA Healthcare Center");

        // Readmission
        String readmission = webDriver.findElement(By.id("hospital_readmission")).getText();
        Assert.assertEquals(readmission, "Yes");

        // Program
        String programText = webDriver.findElement(By.id("program")).getText();
        Assert.assertEquals(programText, "Medicaid");

        // Date
        String dateText = webDriver.findElement(By.id("visit_date")).getText();
        Assert.assertEquals(dateText, "15/12/2022");

        // Comment
        String commentText = webDriver.findElement(By.id("comment")).getText();
        Assert.assertEquals(commentText, "Test");
    }

    // Failed Make Appointment
    @Test(priority = 1)
    public void TC_APPOINTMENT_02() {
        // Make Appointment without input date
        webDriver.findElement(By.id("btn-book-appointment")).click();
        
        // Url remain in make appointment
        String currentURL = webDriver.getCurrentUrl();
        Assert.assertEquals(currentURL, "https://katalon-demo-cura.herokuapp.com/#appointment");
        
        // Button Book Appointment Exist
        boolean exist = !webDriver.findElements(By.id("btn-book-appointment")).isEmpty();
        Assert.assertTrue(exist); // Button Book appoinment exist
    }
}
