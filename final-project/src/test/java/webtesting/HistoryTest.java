package webtesting;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HistoryTest {
    WebDriver webDriver;

    @BeforeClass
    public void openBrowser() {
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    }

    @AfterClass
    public void closeBrowser() throws InterruptedException {
        Thread.sleep(1000);
        webDriver.quit();
    }

    @BeforeMethod
    public void makeAppointment() {
        // Login
        webDriver.get("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        webDriver.findElement(By.id("txt-username")).sendKeys("John Doe");
        webDriver.findElement(By.id("txt-password")).sendKeys("ThisIsNotAPassword");
        webDriver.findElement(By.id("btn-login")).click();

        // Make Appointment
        Select facilitySelect = new Select(webDriver.findElement(By.id("combo_facility")));
        facilitySelect.selectByValue("Hongkong CURA Healthcare Center");
        webDriver.findElement(By.id("chk_hospotal_readmission")).click();
        webDriver.findElement(By.id("radio_program_medicaid")).click();
        webDriver.findElement(By.id("txt_visit_date")).sendKeys("15/12/2022");
        webDriver.findElement(By.id("txt_comment")).sendKeys("Test");
        webDriver.findElement(By.id("btn-book-appointment")).click();
    }

    @AfterMethod
    public void logout() throws InterruptedException {
        Thread.sleep(1000);
        webDriver.get("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
    }

    // Check history after create one appointment, check if element equal to appointment input
    @Test(priority = 0)
    public void TC_HISTORY_01() {
        webDriver.get("https://katalon-demo-cura.herokuapp.com/history.php#history");
        
        // Assertion
        // Header
        String headerText = webDriver.findElement(By.xpath("//*[@id='history']/div/div[1]/div/h2")).getText();
        Assert.assertEquals(headerText, "History");

        // Date
        String dateText = webDriver.findElement(By.className("panel-heading")).getText();
        Assert.assertEquals(dateText, "15/12/2022");

        // Facility
        String facilityText = webDriver.findElement(By.id("facility")).getText();
        Assert.assertEquals(facilityText, "Hongkong CURA Healthcare Center");

        // Readmission
        String readmission = webDriver.findElement(By.id("hospital_readmission")).getText();
        Assert.assertEquals(readmission, "Yes");

        // Program
        String programText = webDriver.findElement(By.id("program")).getText();
        Assert.assertEquals(programText, "Medicaid");

        // Comment
        String commentText = webDriver.findElement(By.id("comment")).getText();
        Assert.assertEquals(commentText, "Test");
    }

    // Check history after make 2 appointment, check appointment count = 2
    @Test(priority = 1)
    public void TC_HISTORY_02() throws InterruptedException {
        // Make New Appointment
        webDriver.get("https://katalon-demo-cura.herokuapp.com/");
        webDriver.findElement(By.id("btn-make-appointment")).click();
        webDriver.findElement(By.id("txt_visit_date")).sendKeys("16/12/2022");

        WebElement buttonAppointment = webDriver.findElement(By.id("btn-book-appointment"));
        ((JavascriptExecutor)webDriver).executeScript("arguments[0].scrollIntoView(true);", buttonAppointment);
        Thread.sleep(1000);
        buttonAppointment.click();

        // Check History
        webDriver.get("https://katalon-demo-cura.herokuapp.com/history.php#history");
        List<WebElement> history = webDriver.findElements(By.className("panel-info"));
        Assert.assertEquals(history.size(), 2);
        
        // Check if appointment input text exist
        boolean date1 = webDriver.getPageSource().contains("16/12/2022");
        Assert.assertTrue(date1); 
        boolean date2 = webDriver.getPageSource().contains("15/12/2022");
        Assert.assertTrue(date2); 
        boolean facility1 = webDriver.getPageSource().contains("Hongkong CURA Healthcare Center");
        Assert.assertTrue(facility1); 
        boolean facility2 = webDriver.getPageSource().contains("Tokyo CURA Healthcare Center");
        Assert.assertTrue(facility2); 
    }
}
